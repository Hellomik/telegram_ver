import { Module } from '@nestjs/common';
// import { TelegramController } from './app.controller';
// import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TelegramEntity } from './telegram/telegram.entity';
import { CodeService } from './code_service';
import { TelegramService } from './app.controller';
import { SendCodeTelegramService } from './send_code_telegram';
import { TelegramModule } from './telegram.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: '195.49.210.250',
      port: 5432,
      password: '6N6FvfsnjN4dgqE',
      username: 'gamerprodb',
      entities: [TelegramEntity],
      database: 'gamerprodb',
      logging: true,
      synchronize: false,
    }),
    TelegramModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
